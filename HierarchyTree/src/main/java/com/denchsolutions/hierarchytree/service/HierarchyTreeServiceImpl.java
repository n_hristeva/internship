package com.denchsolutions.hierarchytree.service;

import com.denchsolutions.hierarchytree.model.EmployeeRecord;

import java.util.HashMap;
import java.util.Map;

public class HierarchyTreeServiceImpl implements HierarchyTreeService {
    private static final String NO_MORE_EMPLOYEES_WITHOUT_MANAGER = "Already have a Employee without a Manager.";
    private final Map<Integer, EmployeeRecord> employees = new HashMap<>();
    private EmployeeRecord withoutManager;

    @Override
    public EmployeeRecord getEmployeeWithoutManager() {
        return this.withoutManager;
    }

    @Override
    public Map<Integer, EmployeeRecord> getEmployees() {
        return employees;
    }

    @Override
    public EmployeeRecord findManager(Map<Integer, EmployeeRecord> map, int id) {
        for (Map.Entry<Integer, EmployeeRecord> item : map.entrySet()) {
            if (item.getKey()
                .equals(id)) {
                return item.getValue();
            }
        }

        return null;
    }

    @Override
    public void createMapper(String[] multiLines) throws IllegalArgumentException {
        EmployeeRecord employeeRecord = null;
        for (String line : multiLines) {
            String[] values = line.split(" ");
            if (values.length >= 3) {
                employeeRecord = new EmployeeRecord(values[0], values[1], values[2]);
            } else {
                employeeRecord = new EmployeeRecord(values[0], values[1], "-1");
            }
            employees.put(employeeRecord.getId(), employeeRecord);
            if (employeeRecord.getManagerId() == -1) {
                if (withoutManager == null) {
                    withoutManager = employeeRecord;
                } else {
                    throw new IllegalArgumentException(NO_MORE_EMPLOYEES_WITHOUT_MANAGER);
                }
            }
        }
    }
}
