package com.denchsolutions.hierarchytree.service;

import com.denchsolutions.hierarchytree.model.EmployeeRecord;

import java.util.Map;

public interface HierarchyTreeService {

    /**
     * @param lines lines to build into map
     */
    void createMapper(String[] lines);

    /**
     * @param map containing all records
     * @param id id of employee, in order to find his/hers manager
     * @return employee manager
     */
    EmployeeRecord findManager(Map<Integer, EmployeeRecord> map, int id);

    /**
     * @return employee without manager (like CEO)
     */
    EmployeeRecord getEmployeeWithoutManager();

    /**
     * @return map of emploees
     */
    Map<Integer, EmployeeRecord> getEmployees();
}
