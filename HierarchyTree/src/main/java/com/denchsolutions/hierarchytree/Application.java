package com.denchsolutions.hierarchytree;

import com.denchsolutions.hierarchytree.model.EmployeeRecord;
import com.denchsolutions.hierarchytree.service.HierarchyTreeService;
import com.denchsolutions.hierarchytree.service.HierarchyTreeServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        String[] lines = {
            "1 Employee1 7",
            "2 Employee2 8",
            "3 Employee1 7",
            "4 Employee1 7",
            "5 Employee5 8",
            "6 CEO" ,
            "7 TeamLead1 6",
            "8 TeamLead2 6",
            "9 CEO"
        };
        HierarchyTreeService hierarchyTreeService = new HierarchyTreeServiceImpl();
        try {
            hierarchyTreeService.createMapper(lines);

        } catch (IllegalArgumentException iae) {
            log.error(iae.getMessage());
        }

        printOutEmployees(hierarchyTreeService.getEmployees(), hierarchyTreeService);
    }

    private static void printOutEmployees(Map<Integer, EmployeeRecord> employees, HierarchyTreeService hierarchyTreeService) {

        for (Map.Entry<Integer, EmployeeRecord> employee : employees.entrySet()) {
            EmployeeRecord e = employee.getValue();
            EmployeeRecord manager = hierarchyTreeService.findManager(employees, e.getManagerId());
            String managerId;
            String managerName;
            if (manager == null) {
                managerId = "null";
                managerName = "null";
            } else {
                managerId = Integer.toString(manager.getId());
                managerName = manager.getName();
            }
            System.out.printf("Employee name: %s,\n" + "Employee id: %d,\n" + "Employee manager id: %s \n" + "Manager name: %s \n" + "\n",
                              e.getName(), employee.getKey(), managerId, managerName);
        }
    }
}
