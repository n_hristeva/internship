package com.denchsolutions.hierarchytree.model;

public class EmployeeRecord {

    private final int id;
    private final int managerId;
    private final String name;

    public EmployeeRecord(String id, String name, String managerId) {
        this.id = Integer.parseInt(id);
        this.name = name;
        this.managerId = Integer.parseInt(managerId);
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public int getManagerId() {
        return this.managerId;
    }

}
