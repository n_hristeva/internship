package com.denchsolutions.searchelement.service;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class SearchElementServiceImplTest {
    private static final ArrayList<String> SIMULATE_USER_INPUT = new ArrayList<>(Arrays.asList("54", "5", "5", "54", "4"));
    private static final String ELEMENT_TO_FIND = "5";
    private static final String ELEMENT_TO_NOT_FIND = "0";
    private static final String FOUND_RESULT = "1";
    private static final String NOT_FOUND_RESULT = "-1";

    private static final SearchElementService searchElementService = new SearchElementServiceImpl();

    @Test
    public void findElementPosition_notFoundElement() {
        final SearchElementService searchElementService = new SearchElementServiceImpl();
        final int foundPosition = searchElementService.findElementPosition(SIMULATE_USER_INPUT, ELEMENT_TO_NOT_FIND);
        assertEquals(NOT_FOUND_RESULT,Integer.toString(foundPosition));
    }


    @Test
    public void findElementPosition_FoundElement() {
        final SearchElementService searchElementService = new SearchElementServiceImpl();
        final int foundPosition = searchElementService.findElementPosition(SIMULATE_USER_INPUT, ELEMENT_TO_FIND);
        assertEquals(FOUND_RESULT,Integer.toString(foundPosition));

    }
}