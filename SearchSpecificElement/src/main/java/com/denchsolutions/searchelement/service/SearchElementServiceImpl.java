package com.denchsolutions.searchelement.service;

import java.util.List;

public final class SearchElementServiceImpl implements SearchElementService {
    @Override
    public int findElementPosition(List<String> listOfElements, String elementToFind) {
        return findPosition(listOfElements, elementToFind);
    }

    private int findPosition(List<String> listOfElements, String elementToFind) {
        int result = -1;
        for (int index = 0; index < listOfElements.size(); index++) {
            String e = listOfElements.get(index);
            if (e.equals(elementToFind)) {
                result = index;
                break;
            }
        }
        return result;
    }
}
