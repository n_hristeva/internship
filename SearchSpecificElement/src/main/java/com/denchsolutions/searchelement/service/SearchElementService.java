package com.denchsolutions.searchelement.service;

import java.util.List;

public interface SearchElementService {
    /**
     * This method find the position of the provided element
     *
     * @param list    to loop thought and search for an element.
     * @param element to find the position of.
     * @return the position of the element.
     */
    int findElementPosition(List<String> list, String element);
}
