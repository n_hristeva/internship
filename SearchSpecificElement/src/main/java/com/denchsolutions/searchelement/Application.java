package com.denchsolutions.searchelement;

import com.denchsolutions.searchelement.service.SearchElementService;
import com.denchsolutions.searchelement.service.SearchElementServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);
    private static final String MORE_PARAMETERS_REQUIRED_ERR = "Entered minimum one parameter.";
    private static final String PARSE_TO_INT_ERR = "Can not parse to integer.";
    private static final String FOUND_OUTPUT = "Number is:'%s' '' found on position '%d'.";
    private static final String NOT_FOUND_OUTPUT = "Number '%s' is not found.";
    //Not clear from where should the searched number be taken from. (Same for 'provided' list).
    private static final String ELEMENT_TO_FIND = "0";

    public static void main(String[] args) {
        try {
            final ArrayList<String> listToSearch = readInput(args);
            final SearchElementService searchElementService = new SearchElementServiceImpl();
            final int foundPosition = searchElementService.findElementPosition(listToSearch, ELEMENT_TO_FIND);

            printOutput(foundPosition);
        } catch (IllegalArgumentException iae) {
            log.error(iae.getMessage());
        }
    }

    private static ArrayList<String> readInput(final String[] args) throws IllegalArgumentException {
        if (args.length < 1) {
            throw new IllegalArgumentException(MORE_PARAMETERS_REQUIRED_ERR);
        }
        ArrayList<String> userInput = new ArrayList<>();
        for (String element : args) {
            if (isOfTypeInteger(element)) {
                userInput.add(element);
            }
        }

        return userInput;
    }

    private static boolean isOfTypeInteger(String s) {
        boolean isInteger = false;
        try {
            Integer.parseInt(s);
            isInteger = true;
        } catch (NumberFormatException ex) {
            log.error(PARSE_TO_INT_ERR);
        }

        return isInteger;
    }

    private static void printOutput(int foundPosition) {
        if (foundPosition != -1) {
            System.out.printf(FOUND_OUTPUT, ELEMENT_TO_FIND, foundPosition);
        } else {
            System.out.printf(NOT_FOUND_OUTPUT, ELEMENT_TO_FIND);
        }
    }
}

