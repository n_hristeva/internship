package com.denchsolutions.animal;

import com.denchsolutions.animal.model.Animal;
import com.denchsolutions.animal.service.AnimalFactoryService;
import com.denchsolutions.animal.service.AnimalFactoryServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {
    private static final String SINGLE_PARAMETER_REQUIRED_ERR = "Enter only one value.";

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        try {
            final String animalType = readInput(args);
            final AnimalFactoryService animalFactoryService = new AnimalFactoryServiceImpl();
            final Animal animal = animalFactoryService.createAnimal(animalType);
            animal.walk();
        } catch (IllegalArgumentException iae) {
            log.error(iae.getMessage());
        }
    }

    private static String readInput(final String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            throw new IllegalArgumentException(SINGLE_PARAMETER_REQUIRED_ERR);
        }

        return args[0];
    }
}

