package com.denchsolutions.animal.model;

public class Dog implements Animal {

    @Override
    public void walk() {
        System.out.println("A dog is walking");
    }
}
