package com.denchsolutions.animal.model;

public class Cat implements Animal {

    @Override
    public void walk() {
        System.out.println("A cat is walking");
    }
}
