package com.denchsolutions.animal.model;

public interface Animal {
    /**
     * This method prints on the console that the animal is walking.
     */
    public void walk();
}
