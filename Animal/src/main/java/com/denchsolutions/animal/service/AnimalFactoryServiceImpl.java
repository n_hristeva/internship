package com.denchsolutions.animal.service;

import com.denchsolutions.animal.model.Animal;
import com.denchsolutions.animal.model.Cat;
import com.denchsolutions.animal.model.Dog;

public final class AnimalFactoryServiceImpl implements AnimalFactoryService {
    private static final String INCORRECT_ANIMAL_TYPE_ERR = "Incorrect Animal type requested.";

    @Override
    public final Animal createAnimal(final String animalType) throws IllegalArgumentException {
        if ("DOG".equalsIgnoreCase(animalType)) {
            return new Dog();
        } else if ("CAT".equalsIgnoreCase(animalType)) {
            return new Cat();
        } else {
            throw new IllegalArgumentException(INCORRECT_ANIMAL_TYPE_ERR);
        }
    }
}
