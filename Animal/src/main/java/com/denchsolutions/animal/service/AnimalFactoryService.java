package com.denchsolutions.animal.service;

import com.denchsolutions.animal.model.Animal;

public interface AnimalFactoryService {

    /**
     * Animal Factory. Instantiates a different cals each time executed, based on provided parameter.
     *
     * @param animalType holds the animal type for instancing.
     * @return instance of type Animal.
     * @throws IllegalArgumentException when match is not found from given parameter.
     */
    Animal createAnimal(final String animalType) throws IllegalArgumentException;
}
