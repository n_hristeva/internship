package com.denchsolutions.animal.service;

import com.denchsolutions.animal.model.Cat;
import com.denchsolutions.animal.model.Dog;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class AnimalFactoryServiceImplTest {

    private static final String INCORRECT_ANIMAL_TYPE_ERR = "Incorrect Animal type requested.";

    //Crooked Strings are given as simulation of user input.
    //In order to check that it will not impact the instantiation of different class types.
    private static final String DOG = "DoG";
    private static final String CAT = "cAT";
    private static final String MISSING_ANIMAL_TYPE = "missing animal type";

    final AnimalFactoryService animalFactoryService = new AnimalFactoryServiceImpl();

    @Test
    public void createAnimal_userInputDog_expectedDogInstance() {
        assertThat(animalFactoryService.createAnimal(DOG), instanceOf(Dog.class));
    }

    @Test
    public void createAnimal_userInputCat_expectedCatInstance() {
        assertThat(animalFactoryService.createAnimal(CAT), instanceOf(Cat.class));
    }

    @Test
    public void createAnimal_userInputMissingAnimalType_throwException() {
        IllegalArgumentException exception =
            assertThrows(IllegalArgumentException.class, () -> animalFactoryService.createAnimal(MISSING_ANIMAL_TYPE));
        assertEquals(INCORRECT_ANIMAL_TYPE_ERR, exception.getMessage());
    }
}