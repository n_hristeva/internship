package com.denchsolutions.searchfrequentelement.service;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class SearchFrequentElementServiceImplTest {

    private static final SearchFrequentElementService SEARCH_FREQUENT_ELEMENT_SERVICE = new SearchFrequentElementServiceImpl();
    private static final ArrayList<Integer> SIMULATE_USER_INPUT = new ArrayList<>(Arrays.asList(-1, -1, 1, 1, 1, 5, 5, 5, 5, 5));
    private static final Map<String, Integer> FREQUENCY_BY_ELEMENTS_OUTPUT = new HashMap<>();
    private static final Map<String, Integer> MOST_FREQUENT_ELEMENT_OUTPUT = new HashMap<>();

    static {
        FREQUENCY_BY_ELEMENTS_OUTPUT.put("1", 3);
        FREQUENCY_BY_ELEMENTS_OUTPUT.put("-1", 2);
        FREQUENCY_BY_ELEMENTS_OUTPUT.put("5", 5);

        MOST_FREQUENT_ELEMENT_OUTPUT.put("number", 5);
        MOST_FREQUENT_ELEMENT_OUTPUT.put("frequency", 5);
    }

    @Test
    public void findFrequencyByElements_correctInput_correctOutput() {
        final Map<String, Integer> frequencyByElements = SEARCH_FREQUENT_ELEMENT_SERVICE.findFrequencyByElements(SIMULATE_USER_INPUT);
        assertEquals(FREQUENCY_BY_ELEMENTS_OUTPUT, frequencyByElements);
    }

    @Test
    public void findMostFrequentElement_correctInput_correctOutput() {
        final Map<String, Integer> mostFrequentElement = SEARCH_FREQUENT_ELEMENT_SERVICE.findMostFrequentElement(SIMULATE_USER_INPUT);
        assertEquals(MOST_FREQUENT_ELEMENT_OUTPUT, mostFrequentElement);
    }
}