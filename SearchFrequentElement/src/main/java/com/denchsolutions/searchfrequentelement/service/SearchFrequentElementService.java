package com.denchsolutions.searchfrequentelement.service;

import java.util.List;
import java.util.Map;

public interface SearchFrequentElementService {
    /**
     * This method provides a Map Collection containing all the integers and their frequencies.
     *
     * @param listToSearch list of integer values.
     * @return type Map Collection containing keys like: '-1' = 3.
     */
    public Map<String, Integer> findFrequencyByElements(List<Integer> listToSearch);

    /**
     * This method provides a Map Collection containing two keys:
     * number     - the most frequent found integer.
     * frequency  - how many times was the integer found.
     *
     * @param listToSearch list of integer values.
     * @return type Map Collection containing keys: 'number' & 'frequency'.
     */
    public Map<String, Integer> findMostFrequentElement(List<Integer> listToSearch);
}