package com.denchsolutions.searchfrequentelement.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public final class SearchFrequentElementServiceImpl implements SearchFrequentElementService {

    @Override
    public Map<String, Integer> findFrequencyByElements(List<Integer> listToSearch) {
        return findFrequencyForEachNumber(listToSearch);
    }

    @Override
    public Map<String, Integer> findMostFrequentElement(List<Integer> listToSearch) {
        return mostFrequentNumber(findFrequencyByElements(listToSearch));
    }

    private Map<String, Integer> findFrequencyForEachNumber(List<Integer> listToSearch) {
        Map<String, Integer> frequencyMapper = new HashMap<>();
        for (int number : listToSearch) {
            String key = Integer.toString(number);
            if (frequencyMapper.containsKey(key)) {
                int frequency = frequencyMapper.get(key);
                frequency++;
                frequencyMapper.put(key, frequency);
            } else {
                frequencyMapper.put(key, 1);
            }
        }

        return frequencyMapper;
    }

    private Map<String, Integer> mostFrequentNumber(Map<String, Integer> frequencyMapper) {
        Map<String, Integer> mostFrequentNumber = new HashMap<>();
        int frequent = 0;
        String mostFrequentKey = "";

        for (Entry<String, Integer> element : frequencyMapper.entrySet()) {
            if (frequent < element.getValue()) {
                mostFrequentKey = element.getKey();
                frequent = element.getValue();
            }
        }
        mostFrequentNumber.put("number", Integer.parseInt(mostFrequentKey));
        mostFrequentNumber.put("frequency", frequent);

        return mostFrequentNumber;
    }
}
