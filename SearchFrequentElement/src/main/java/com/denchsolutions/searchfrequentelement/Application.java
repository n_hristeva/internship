package com.denchsolutions.searchfrequentelement;

import com.denchsolutions.searchfrequentelement.service.SearchFrequentElementService;
import com.denchsolutions.searchfrequentelement.service.SearchFrequentElementServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;


public class Application {
    private static final String MORE_PARAMETERS_REQUIRED_ERR = "Entered minimum one parameter.";
    private static final String PARSE_TO_INT_ERR = "Can not parse to integer.";
    private static final String FINAL_OUTPUT = "Most frequent number is:'%d' found '%d' times.";
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            final ArrayList<Integer> listToSearch = readInput(args);
            final SearchFrequentElementService searchFrequentElementService = new SearchFrequentElementServiceImpl();


            final Map<String, Integer> frequencyElementMapper = searchFrequentElementService.findFrequencyByElements(listToSearch);
            final Map<String, Integer> mostFrequentElement = searchFrequentElementService.findMostFrequentElement(listToSearch);



            printOutMapper(frequencyElementMapper);
            System.out.printf(FINAL_OUTPUT, mostFrequentElement.get("number"), mostFrequentElement.get("frequency"));
        } catch (NumberFormatException nfe) {
            log.error(PARSE_TO_INT_ERR);
        } catch (IllegalArgumentException iae) {
            log.error(iae.getMessage());
        }
    }

    private static ArrayList<Integer> readInput(final String[] args) throws IllegalArgumentException {
        if (args.length < 1) {
            throw new IllegalArgumentException(MORE_PARAMETERS_REQUIRED_ERR);
        }
        ArrayList<Integer> userInput = new ArrayList<>();
        for (String element : args) {
            userInput.add(Integer.parseInt(element));
        }

        return userInput;
    }

    private static void printOutMapper(Map<String, Integer> frequencyElementMapper) {
        for (Map.Entry<String, Integer> element : frequencyElementMapper.entrySet()) {
            System.out.println(element.getKey() + ":" + element.getValue()
                .toString());
        }
    }
}

