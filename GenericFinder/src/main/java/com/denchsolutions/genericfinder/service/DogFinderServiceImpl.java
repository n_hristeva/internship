package com.denchsolutions.genericfinder.service;

import com.denchsolutions.genericfinder.model.Dog;

public class DogFinderServiceImpl implements FinderService<Dog, Integer> {

    @Override
    public Dog find(Integer age) {
        return new Dog(age);
    }
}
