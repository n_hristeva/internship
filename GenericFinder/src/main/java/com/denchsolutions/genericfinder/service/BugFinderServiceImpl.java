package com.denchsolutions.genericfinder.service;

import com.denchsolutions.genericfinder.model.Bug;

public class BugFinderServiceImpl implements FinderService<Bug, String> {

    @Override
    public Bug find(String description) {
        return new Bug(description);
    }
}
