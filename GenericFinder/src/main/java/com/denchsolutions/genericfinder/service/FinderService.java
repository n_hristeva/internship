package com.denchsolutions.genericfinder.service;

public interface FinderService<T, E> {
    /**
     * @return 'T' type object
     */
    T find(E value);
}
