package com.denchsolutions.genericfinder;

import com.denchsolutions.genericfinder.model.Bug;
import com.denchsolutions.genericfinder.model.Dog;
import com.denchsolutions.genericfinder.service.BugFinderServiceImpl;
import com.denchsolutions.genericfinder.service.DogFinderServiceImpl;
import com.denchsolutions.genericfinder.service.FinderService;

public class Application {
    public static void main(String[] args) {
        FinderService<Dog, Integer> dogFinderService = new DogFinderServiceImpl();
        FinderService<Bug, String> bugFinderService = new BugFinderServiceImpl();

        Dog d = dogFinderService.find(2);
        System.out.println(d.getAge());

        Bug b = bugFinderService.find("Little bug.");
        System.out.println(b.getDescription());
    }
}