package com.denchsolutions.genericfinder.model;

public class Bug {

    private String description;

    public Bug(String description) {
        this.setDescription(description);
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
