package com.denchsolutions.classreflection;

import com.denchsolutions.classreflection.model.Person;
import com.denchsolutions.classreflection.util.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;

import static java.lang.System.out;

public class Application {

    public static void main(String[] args)
        throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        ReflectionUtils.printClassFields(Person.class);
        Person person = ReflectionUtils.createWithReflection(Person.class);
        out.println(person);
        Person personUpdated = ReflectionUtils.setFieldValueWithReflection(person, "name", "Test");
        out.println(personUpdated);
    }
}
