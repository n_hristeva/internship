package com.denchsolutions.classreflection.enums;

public enum CountryType {
    BG("Bulgaria"), US("United States"), UK("United Kingdom");

    private final String name;

    CountryType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
