package com.denchsolutions.classreflection.model;

import com.denchsolutions.classreflection.enums.CountryType;

public class Person {
    private int age;
    private String name;
    private CountryType country;

    public Person() {}

    public Person(int age, String name, CountryType country) {
        this.age = age;
        this.name = name;
        this.country = country;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryType getCountry() {
        return country;
    }

    public void setCountry(CountryType country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Person{" + "age=" + this.age + ", name='" + this.name + '\'' + ", country=" + this.country + '}';
    }
}
