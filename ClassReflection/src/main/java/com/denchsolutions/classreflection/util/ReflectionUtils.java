package com.denchsolutions.classreflection.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import static java.lang.System.out;

public final class ReflectionUtils {
    private ReflectionUtils() {
        throw new AssertionError("ReflectionUtils should not be instantiated.");
    }

    public static void printClassFields(Class<?> clazz) {
        Field[] allFields = clazz.getDeclaredFields();

        for (Field field : allFields) {
            String name = field.getName();
            String type = field.getType()
                .getSimpleName();
            out.printf("Field: '%s' - Type: '%s'%n", name, type);
        }
    }

    public static <T> T createWithReflection(Class<T> clazz)
        throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        return clazz.getConstructor()
            .newInstance();
    }

    public static <T> T setFieldValueWithReflection(T object, String fieldName, String value) throws IllegalAccessException, NoSuchFieldException {
        Field field = object.getClass()
            .getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(object, value);

        return object;
    }
}
