package com.denchsolutions.serializer.enums;

public enum DataType {
    /*
    import com.denchsolutions.serializer.fileformat.CSV;
    import com.denchsolutions.serializer.fileformat.JSON;
    import com.denchsolutions.serializer.fileformat.XML;

    JSON(JSON.class.getName()), CSV(CSV.class.getName()), XML(XML.class.getName());
     */

    JSON("JSON"), CSV("CSV"), XML("XML");
    private final String type;

    public String getDataType() {
        return type;
    }

    DataType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return this.getDataType();
    }
}
