package com.denchsolutions.serializer.service;

import com.denchsolutions.serializer.enums.DataType;

import java.lang.reflect.InvocationTargetException;

public interface SerializeService<T> {

    /**
     * @param t any type of instance that wants to be serialized.
     * @param type the required format.
     */
    void serialize(T t, DataType type) throws IllegalAccessException;

    /**
     * @param data information that will be instantiated.
     * @param type the required format.
     * @return instance based on clazz and data.
     */
    T deserialize(String data, DataType type, Class<T> clazz)
        throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
