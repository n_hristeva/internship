package com.denchsolutions.serializer.service;

import com.denchsolutions.serializer.enums.DataType;
import com.denchsolutions.serializer.fileformat.CSV;
import com.denchsolutions.serializer.fileformat.JSON;
import com.denchsolutions.serializer.fileformat.XML;

import java.lang.reflect.InvocationTargetException;

public class SerializeServiceImpl<T> implements SerializeService<T> {
    @Override
    public void serialize(T t, DataType type)  {
        if (type == DataType.JSON) {
            try {
                new JSON<T>().serialize(t);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (type == DataType.CSV) {
            try {
                new CSV<T>().serialize(t);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (type == DataType.XML) {
            try {
                new XML<T>().serialize(t);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public T deserialize(String data, DataType type, Class<T> clazz){
        T t = null;
        if (type == DataType.JSON) {
            t = new JSON<T>().deserialize(data, clazz);
        } else if (type == DataType.CSV) {
            t = new CSV<T>().deserialize(data, clazz);
        } else if (type == DataType.XML) {
            t = new XML<T>().deserialize(data, clazz);
        }

        return t;
    }
}


