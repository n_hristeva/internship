package com.denchsolutions.serializer.model;

public class Person {
    public int age;
    public String name;
    public String countryType;

    public Person(String name, int age, String countryType) {
        this.name = name;
        this.age = age;
        this.countryType = countryType;
    }

    public Person(){
        name = "";
        age = 0;
        countryType = "";
    }

    @Override
    public String toString() {
        return "Person{" + "age=" + age + ", name='" + name + '\'' + ", countryType='" + countryType + '\'' + '}';
    }
}
