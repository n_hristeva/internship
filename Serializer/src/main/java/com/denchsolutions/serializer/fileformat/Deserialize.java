package com.denchsolutions.serializer.fileformat;

import java.lang.reflect.InvocationTargetException;

public interface Deserialize<T> {

    /**
     * @param data contains fields and values that must be set.
     * @return instance of T with the set data.
     */
    T deserialize(String data, Class<T> clazz)
        throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException;
}
