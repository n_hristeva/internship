package com.denchsolutions.serializer.fileformat;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class XML<T> implements Serialize<T>, Deserialize<T> {
    @Override
    public void serialize(T t) throws IllegalAccessException {
        String xml = String.format("<" + "%s>%n", t.getClass()
            .getSimpleName());
        Field[] allFields = t.getClass()
            .getDeclaredFields();

        for (Field field : allFields) {
            Object fieldValue = field.get(t);
            String name = field.getName();
            xml = xml.concat(String.format("<%s>%s</%s>%n", name, fieldValue, name));
        }

        xml = xml.concat(String.format("</" + "%s>%n", t.getClass()
            .getSimpleName()));
        System.out.println(xml);
    }

    @Override
    public T deserialize(String data, Class<T> clazz) {
        T t = null;
        try {
            t = clazz.getConstructor()
                .newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        Field[] allFields = clazz.getDeclaredFields();
        data = data.trim();
        String[] fieldWithValues = data.split("\n");
        fieldWithValues = Arrays.copyOfRange(fieldWithValues, 1, fieldWithValues.length - 1);

        for (String fieldWithValue : fieldWithValues) {
            fieldWithValue = fieldWithValue.replaceAll("[<->]", "");
            String[] rowData = fieldWithValue.split("/");
            String fieldName = rowData[1].trim();
            setInstanceField(t, fieldName, allFields, rowData);
        }

        return t;
    }

    private void setInstanceField(T t, String fieldName, Field[] allFields, String[] rowData) {
        String name;
        String type;
        for (Field field : allFields) {
            name = field.getName();
            if (fieldName.equals(name)) {
                type = field.getType()
                    .getSimpleName();
                String value = rowData[0].trim();
                value = value.replaceAll(fieldName, "");
                try {
                    switch (type) {
                        case "String" -> field.set(t, value);
                        case "int" -> field.set(t, Integer.parseInt(value));
                        case "long" -> field.set(t, Long.parseLong(value));
                        default -> field.set(t, value);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}
