package com.denchsolutions.serializer.fileformat;

public interface Serialize<T> {

    /**
     * @param t can be any type of instance.
     * Print in console the serialized instance.
     */
    void serialize(T t) throws IllegalAccessException;
}
