package com.denchsolutions.serializer.fileformat;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class CSV<T> implements Serialize<T>, Deserialize<T> {
    //For simplicity, please have in mind only classes with simple properties – int, long, String
    @Override
    public void serialize(T t) throws IllegalAccessException {
        String csv = String.format("%s%n", t.getClass()
            .getSimpleName());
        String fieldNames = "";
        String fieldValues = "";
        Field[] allFields = t.getClass()
            .getDeclaredFields();
        int fieldsLength = allFields.length;
        int fieldsLengthMinusOne = fieldsLength - 1;

        for (int i = 0; i < fieldsLength; i++) {
            Field field = allFields[i];
            Object fieldValue = field.get(t);
            String name = field.getName();

            if (i != fieldsLengthMinusOne) {
                fieldNames = fieldNames.concat(String.format("%s,", name));
                fieldValues = fieldValues.concat(String.format("%s,", fieldValue));
            } else {
                fieldNames = fieldNames.concat(String.format("%s%n", name));
                fieldValues = fieldValues.concat(String.format("%s%n", fieldValue));
            }
        }

        csv = csv.concat(fieldNames)
            .concat(fieldValues);
        System.out.println(csv);
    }

    @Override
    public T deserialize(String data, Class<T> clazz) {
        String fieldName;
        String fieldValue;
        T t = null;
        try {
            t = clazz.getConstructor()
                .newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        Field[] allFields = clazz.getDeclaredFields();
        data = data.trim();
        String[] fieldWithValuesAndNames = data.split("\n");
        String[] fieldNames = fieldWithValuesAndNames[0].split(",");
        String[] fieldValues = fieldWithValuesAndNames[1].split(",");

        for (int i = 0; i < fieldNames.length; i++) {
            fieldName = fieldNames[i];
            fieldValue = fieldValues[i];
            setInstanceField(t, fieldName, fieldValue, allFields);
        }

        return t;
    }

    private void setInstanceField(T t, String fieldName, String fieldValue, Field[] allFields) {
        String name;
        String type;
        for (Field field : allFields) {
            name = field.getName();
            if (fieldName.equals(name)) {
                type = field.getType()
                    .getSimpleName();
                try {
                    switch (type) {
                        case "String" -> field.set(t, fieldValue);
                        case "int" -> field.set(t, Integer.parseInt(fieldValue));
                        case "long" -> field.set(t, Long.parseLong(fieldValue));
                        default -> field.set(t, fieldValue);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                break;
            }
        }
    }
}

