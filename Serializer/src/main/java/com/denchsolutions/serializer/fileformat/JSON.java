package com.denchsolutions.serializer.fileformat;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class JSON<T> implements Serialize<T>, Deserialize<T> {
    @Override
    public void serialize(T t) throws IllegalAccessException {
        String json = String.format("{" + "\"%s\": " + "{%n", t.getClass()
            .getSimpleName());
        Field[] allFields = t.getClass()
            .getDeclaredFields();
        int fieldsLength = allFields.length;
        int fieldsLengthMinusOne = fieldsLength - 1;

        for (int i = 0; i < fieldsLength; i++) {
            Field field = allFields[i];
            Object fieldValue = field.get(t);
            String name = field.getName();
            String type = field.getType()
                .getSimpleName();
            if (type.equals("String")) {
                json = json.concat(String.format("\"%s\": \"%s\"", name, fieldValue));
            } else {
                json = json.concat(String.format("\"%s\": %s", name, fieldValue));
            }
            if (i != fieldsLengthMinusOne) {
                json = json.concat(String.format(",%n"));
            } else {
                json = json.concat(String.format("%n}"));
            }
        }

        json = json.concat("}");
        System.out.println(json);
    }

    @Override
    public T deserialize(String data, Class<T> clazz) {
        T t = null;
        try {
            t = clazz.getConstructor()
                .newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        Field[] allFields = clazz.getDeclaredFields();
        data = data.trim();
        data = data.substring(1, data.length() - 1);
        String[] fieldWithValues = data.split(",");
        for (String fieldWithValue : fieldWithValues) {
            String[] rowData = fieldWithValue.split(":");
            String fieldName = rowData[0].trim();
            fieldName = fieldName.substring(1, fieldName.length() - 1);
            setInstanceField(t, fieldName, allFields, rowData);
        }

        return t;
    }

    private void setInstanceField(T t, String fieldName, Field[] allFields, String[] rowData) {
        String name;
        String type;
        for (Field field : allFields) {
            name = field.getName();
            if (fieldName.equals(name)) {
                type = field.getType()
                    .getSimpleName();
                String value = rowData[1].trim();
                try {
                    switch (type) {
                        case "String" -> {
                            value = value.substring(1, value.length() - 1);
                            field.set(t, value);
                        }
                        case "int" -> field.set(t, Integer.parseInt(value));
                        case "long" -> field.set(t, Long.parseLong(value));
                        default -> field.set(t, value);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                break;
            }
        }
    }
}
