package com.denchsolutions.serializer;

import com.denchsolutions.serializer.enums.DataType;
import com.denchsolutions.serializer.model.Person;
import com.denchsolutions.serializer.service.SerializeService;
import com.denchsolutions.serializer.service.SerializeServiceImpl;

import java.lang.reflect.InvocationTargetException;

public class Application {
    public static void main(String[] args)
        throws IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException {
        SerializeService<Person> serializeService = new SerializeServiceImpl<>();

        Person personToSerialize = new Person("PersonOne", 2, "UK");
        serializeService.serialize(personToSerialize, DataType.JSON);
        serializeService.serialize(personToSerialize, DataType.XML);
        serializeService.serialize(personToSerialize, DataType.CSV);

        String dataJSON = "{" + "\"name\":\"PersonOneDes\"," + "\"age\":2," + "\"countryType\":\"UK\"" + "}";
        System.out.println(dataJSON);
        String dataXML = """
            <Person>
            <age>2</age>
            <name>PersonOneDes</name>
            <countryType>UK</countryType>
            </Person>
            """;
        String dataCSV = """
            age,name,countryType
            2,PersonOneDes,UK
            """;
        System.out.println(serializeService.deserialize(dataJSON, DataType.JSON, Person.class));
        System.out.println(serializeService.deserialize(dataXML, DataType.XML, Person.class));
        System.out.println(serializeService.deserialize(dataCSV, DataType.CSV, Person.class));
    }
}
