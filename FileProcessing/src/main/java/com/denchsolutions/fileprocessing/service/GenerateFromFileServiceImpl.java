package com.denchsolutions.fileprocessing.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

public class GenerateFromFileServiceImpl implements GenerateFromFileService<Integer> {

    @Override
    public ArrayList<Integer> createArrayFromFile(String fileName) {
        final String directory = Paths.get("").toAbsolutePath() + fileName;
        ArrayList<Integer> list = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(directory));) {
            return fillUpArray(list, bufferedReader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    private ArrayList<Integer> fillUpArray(ArrayList<Integer> list, BufferedReader bufferedReader) {
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                list.add(Integer.parseInt(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }
}
