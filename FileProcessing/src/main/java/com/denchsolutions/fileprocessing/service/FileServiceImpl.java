package com.denchsolutions.fileprocessing.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.lang.System.out;

public class FileServiceImpl implements FileService {
    private static final String DEVIATION_OUTPUT = "for value '%d' deviation is '%.2f'.\n";

    @Override
    public void printFromFile(final String fileName) {
        final String directory = Paths.get("").toAbsolutePath() + fileName;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(directory))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeInSameFileDeviationPerNumber(final String fileName) {
        final GenerateFromFileService<Integer> generateFromFileService = new GenerateFromFileServiceImpl();
        ArrayList<Integer> numArray = generateFromFileService.createArrayFromFile(fileName);
        Map<Integer, Double> deviationMapper = new HashMap<>();

        double result;
        double totalSum = 0.0;
        double standardDeviation = 0.0;
        double length = numArray.size();

        for (Integer value : numArray) {
            totalSum += value;
        }
        double average = totalSum / length;
        for (Integer value : numArray) {
            standardDeviation += Math.pow(value - average, 2);
            result = Math.sqrt(standardDeviation / length);
            deviationMapper.put(value, result);
        }

        writeInFile(fileName, deviationMapper);
    }

    private void writeInFile(final String fileName, final Map<Integer, Double> deviationMapper) {
        final String directory = Paths.get("").toAbsolutePath() + fileName;

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(directory));) {
            for (Map.Entry<Integer, Double> m : deviationMapper.entrySet()) {
                String lineToAppend = String.format(DEVIATION_OUTPUT, m.getKey(), m.getValue());
                bufferedWriter.write(lineToAppend);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



