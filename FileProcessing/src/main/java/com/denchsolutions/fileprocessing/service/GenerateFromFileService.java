package com.denchsolutions.fileprocessing.service;

import java.util.ArrayList;

/**
 * This interface contains logic connected with creation of different structures of any type.
 *
 * @param <T> can be Integer. Can be expanded to take Double, String and other types.
 */
public interface GenerateFromFileService<T> {
    /**
     * Generated from provided file name.
     *
     * @param fileName Needed file name in order to find the file.
     * @return ArrayList<T> generated from provided file name.
     */
     ArrayList<T> createArrayFromFile(String fileName);
}
