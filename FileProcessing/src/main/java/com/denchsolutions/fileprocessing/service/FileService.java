package com.denchsolutions.fileprocessing.service;

public interface FileService {

    /**
     * Printing the lines in provided file name.
     *
     * @param fileName Needed file name in order to find the file.
     */
     void printFromFile(final String fileName);

    /**
     * Write in provided file deviation rer number and save in same file.
     *
     * @param fileName Needed file name in order to find the file.
     */
     void writeInSameFileDeviationPerNumber(final String fileName);
}
