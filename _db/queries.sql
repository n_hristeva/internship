CREATE TABLE IF NOT EXISTS employees (
	id INT GENERATED ALWAYS AS IDENTITY,
	employe_name VARCHAR(255) NOT NULL,
	manager_id INT,
	salary INT NOT NULL,
	team_id INT,
	PRIMARY KEY(id)
);
	   
INSERT INTO employees(employe_name, manager_id, salary, team_id)
VALUES('Michael',NULL,8000,NULL),
      ('George',1,6000,1),
      ('Greg',1,5000, 1),
	  ('Peter',2,2000,2),
      ('John',2,3500,3),
	  ('Hans',2,3000,2),
      ('Jack',2,3000,4),
	  ('Hugh',3,1500,5);
	  
--1)list all employees who are CEOs (i.e. don't have a manager)

SELECT id, employe_name, manager_id, salary, team_id
FROM employees
WHERE manager_id is NULL;


--2)list all employee names with the names of their managers (NOTE exclude CEOs)

SELECT  e.employe_name, (SELECT employe_name FROM employees WHERE id = e.manager_id) as manager_name
FROM employees e
WHERE manager_id IS NOT NULL;


--3)list the employee (that is not a manager) with the second largest salary

SELECT 
FROM employees 
WHERE id NOT IN(SELECT DISTINCT manager_id FROM employees WHERE manager_id IS NOT NULL)
ORDER BY salary DESC
LIMIT 2 OFFSET 2;


--4)list the manager (exclude CEO) with the second largest salary

SELECT 
FROM employees 
WHERE id IN(SELECT DISTINCT manager_id FROM employees WHERE manager_id IS NOT NULL)
ORDER BY salary DESC
LIMIT 1 OFFSET 1;


--5) get the sum of salaries for all employees that are not managers

SELECT  SUM(salary) 
FROM employees 
WHERE id NOT IN(SELECT DISTINCT manager_id FROM employees WHERE manager_id IS NOT NULL);


--6) get the sum of salaries for all 'managers' (manager manages at least one employee). exclude the CEO (Michael).

SELECT SUM(salary)
FROM employees
WHERE id IN(
    SELECT DISTINCT manager_id FROM employees
        WHERE manager_id IS NOT NULL
          AND manager_id <>(SELECT id FROM employees WHERE manager_id is NULL)
);


--7) print all distinct team_ids with the number of employees that are part of them and that have salary greater than 2500.

SELECT DISTINCT e.team_id ,
	  (
      SELECT COUNT()
	  FROM   employees 
	  WHERE team_id = e.team_id  AND salary >2500
	  )
FROM employees e
WHERE team_id IS NOT NULL;

--8) write a statement to add a column 'archived' to the employee table. It can have values '0' or '1' ('0' by default, null not permitted).

ALTER TABLE employees 
ADD archived INT DEFAULT 0 NOT NULL
CONSTRAINT CK_archived CHECK (archived IN (0, 1));

--9) create another table 'company' which has id & name. add a column 'company_id' to employee table. 'company_id' is a foreign key to the company table

CREATE TABLE IF NOT EXISTS companies (
	id INT GENERATED ALWAYS AS IDENTITY,
	PRIMARY KEY(id)
	
);

ALTER TABLE employees
ADD COLUMN company_id INT;

ALTER TABLE employees
 ADD CONSTRAINT fk_company FOREIGN KEY (company_id) REFERENCES companies (id) MATCH FULL;


--10) increase the salary of all employees which have 'Michael' as manager by 200

UPDATE employees
SET salary = salary + 200
WHERE manager_id = (SELECT id FROM employees WHERE employe_name='Michael');


--11) delete 'Greg' and all employees that he manages.

DELETE FROM employees 
WHERE id IN(
	(SELECT id FROM employees WHERE manager_id =(SELECT id FROM employees WHERE employe_name='Greg')),
    (SELECT id FROM employees WHERE employe_name='Greg')
) 
RETURNING * ;


--12) drop 'company' table.

DROP TABLE if exists companies cascade;


--13) Design a database for storing personal information - personal info

CREATE DATABASE person_info
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United Kingdom.1252'
    LC_CTYPE = 'English_United Kingdom.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- NOTE: bigserial is big int + auto increment + not null !!
CREATE TABLE IF NOT EXISTS people (
	id bigserial primary key,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255)  NOT NULL,
	email VARCHAR(255) UNIQUE NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS addresses (
	id INT GENERATED ALWAYS AS IDENTITY,
	address VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS person_address (
	person_id INT NOT NULL,
	addresse_id INT NOT NULL
);

ALTER TABLE person_address
 ADD CONSTRAINT fk_perosn FOREIGN KEY (person_id) REFERENCES people (id) MATCH FULL,
 ADD CONSTRAINT fk_addres FOREIGN KEY (addresse_id) REFERENCES addresses (id) MATCH FULL;


CREATE TABLE IF NOT EXISTS employments (
	id INT GENERATED ALWAYS AS IDENTITY,
	job_title VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS person_employment (
	person_id INT NOT NULL,
	employment_id INT NOT NULL
);

ALTER TABLE person_employment
 ADD CONSTRAINT fk_perosn FOREIGN KEY (person_id) REFERENCES people (id) MATCH FULL,
 ADD CONSTRAINT fk_employment FOREIGN KEY (employment_id) REFERENCES employments (id) MATCH FULL;

CREATE TABLE IF NOT EXISTS social_network_profiles (
	id INT GENERATED ALWAYS AS IDENTITY,
	name VARCHAR(255) NOT NULL,
	person_id INT NOT NULL,
	PRIMARY KEY(id)
);

ALTER TABLE social_network_profiles
 ADD CONSTRAINT fk_perosn FOREIGN KEY (person_id) REFERENCES people (id) MATCH FULL;

CREATE USER superuser1 WITH PASSWORD 'super1';
GRANT ALL PRIVILEGES ON DATABASE person_info to superuser1;