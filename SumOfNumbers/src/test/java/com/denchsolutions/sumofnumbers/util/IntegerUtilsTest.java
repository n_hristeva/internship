package com.denchsolutions.sumofnumbers.util;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class IntegerUtilsTest {
    private static final int MIN_NUMBER = 1;
    private static final int UP_TO = 5;
    private static final int EXPECTED_RESULT = 15;

    @Test
    public void calculateSumOfPermutationUnitsWithWhile_correctInput_correctOutput() {
        assertEquals(EXPECTED_RESULT, IntegerUtils.calculateSumOfPermutationUnitsWithWhile(MIN_NUMBER, UP_TO));
    }

    @Test
    public void calculateSumOfPermutationUnitsWithDoWhile_correctInput_correctOutput() {
        assertEquals(EXPECTED_RESULT, IntegerUtils.calculateSumOfPermutationUnitsWithDoWhile(MIN_NUMBER, UP_TO));
    }

    @Test
    public void calculateSumOfPermutationUnitsWithFor_correctInput_correctOutput() {
        assertEquals(EXPECTED_RESULT, IntegerUtils.calculateSumOfPermutationUnitsWithFor(MIN_NUMBER, UP_TO));
    }
}