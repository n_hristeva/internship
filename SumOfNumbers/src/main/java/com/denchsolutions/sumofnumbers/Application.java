package com.denchsolutions.sumofnumbers;

import com.denchsolutions.sumofnumbers.util.IntegerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    private static final int MIN_NUMBER = 1;
    private static final String PARSE_TO_INT_ERR = "Argument '{}' can not be parsed to integer.";
    private static final String BIGGER_THAN_ONE_ERR = "Number must be greater than %d.";
    private static final String SINGLE_PARAMETER_REQUIRED_ERR = "Enter only one value.";

    public static void main(String[] args) {
        try {
            final int number = readInput(args);
            final int calculatedSum = IntegerUtils.calculateSumOfPermutationUnitsWithFor(MIN_NUMBER, number);

            System.out.println(calculatedSum);
        } catch (NumberFormatException nfe) {
            log.error(PARSE_TO_INT_ERR, args[0]);
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
        }
    }

    private static int readInput(final String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            throw new IllegalArgumentException(SINGLE_PARAMETER_REQUIRED_ERR);
        }
        final int number = Integer.parseInt(args[0]);
        if (number < MIN_NUMBER) {
            throw new IllegalArgumentException(String.format(BIGGER_THAN_ONE_ERR, MIN_NUMBER));
        }

        return number;
    }
}
