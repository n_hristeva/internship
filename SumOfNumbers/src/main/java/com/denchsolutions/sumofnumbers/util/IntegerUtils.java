package com.denchsolutions.sumofnumbers.util;

public class IntegerUtils {
    private static final String DO_NOT_INSTANTIATE_ERR = "Utility class can not be instantiated.";

    private IntegerUtils() {
        throw new IllegalStateException(DO_NOT_INSTANTIATE_ERR);
    }

    public static int calculateSumOfPermutationUnitsWithWhile(final int minNumber, int number) {
        int sum = minNumber;
        int counter = minNumber;

        while (number > counter) {
            counter++;
            sum += counter;
        }

        return sum;
    }

    public static int calculateSumOfPermutationUnitsWithDoWhile(final int minNumber, int number) {
        int sum = minNumber;
        int counter = minNumber;

        do {
            counter++;
            sum += counter;
        } while (number > counter);

        return sum;
    }

    public static int calculateSumOfPermutationUnitsWithFor(final int minNumber, int number) {
        int sum = 0;

        for (int counter = minNumber; counter <= number; counter++) {
            sum += counter;
        }

        return sum;
    }
}
