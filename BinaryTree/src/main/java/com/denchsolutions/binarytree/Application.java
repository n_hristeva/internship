package com.denchsolutions.binarytree;

import com.denchsolutions.binarytree.model.Node;
import com.denchsolutions.binarytree.service.BinaryTreeService;
import com.denchsolutions.binarytree.service.BinaryTreeServiceImpl;

import java.util.List;

public class Application {

    private static final String BINARY_TREE_SERVICE_TRUE = "Is а Binary Search Tree.";
    private static final String BINARY_TREE_SERVICE_FALSE = "Not a Binary Search Tree.";

    public static void main(String[] args) {
        BinaryTreeService binaryTreeService = new BinaryTreeServiceImpl();

        notABinarySearchTree(binaryTreeService);
        isABinarySearchTree(binaryTreeService);
        zigZagTraversal(binaryTreeService);
    }

    private static void notABinarySearchTree(BinaryTreeService binaryTreeService) {
        Node isNotBSTRoot = new Node(3);
        isNotBSTRoot.setLeft(new Node(2));
        isNotBSTRoot.setRight(new Node(5));
        isNotBSTRoot.getLeft()
            .setLeft(new Node(1));
        isNotBSTRoot.getLeft()
            .setRight(new Node(4));

        printOutput(binaryTreeService.isBinarySearchTree(isNotBSTRoot));
    }

    private static void isABinarySearchTree(BinaryTreeService binaryTreeService) {
        Node isBSTRoot = new Node(4);
        isBSTRoot.setLeft(new Node(2));
        isBSTRoot.setRight(new Node(5));
        isBSTRoot.getLeft()
            .setLeft(new Node(1));
        isBSTRoot.getLeft()
            .setRight(new Node(3));

        printOutput(binaryTreeService.isBinarySearchTree(isBSTRoot));
    }

    private static void zigZagTraversal(BinaryTreeService binaryTreeService) {
        Node root = new Node(1);
        root.setLeft(new Node(2));
        root.setRight(new Node(3));
        root.getLeft()
            .setLeft(new Node(7));
        root.getLeft()
            .setRight(new Node(6));
        root.getRight()
            .setLeft(new Node(5));
        root.getRight()
            .setRight(new Node(4));

        printBinaryTree(binaryTreeService.zigZagTraversal(root));
    }

    private static void printOutput(boolean result) {
        if (result) {
            System.out.println(BINARY_TREE_SERVICE_TRUE);
        } else {
            System.out.println(BINARY_TREE_SERVICE_FALSE);
        }
    }

    private static void printBinaryTree(List<Integer> tree) {
        for (Integer node : tree) {
            System.out.print(node + " ");
        }
    }
}
