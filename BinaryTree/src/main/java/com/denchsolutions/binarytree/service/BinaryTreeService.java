package com.denchsolutions.binarytree.service;

import com.denchsolutions.binarytree.model.Node;

import java.util.ArrayList;

public interface BinaryTreeService {

    /**
     * This method can define if a Tree is a Binary Search Tree.
     * @return true or false value depending on the execution.
     */
    boolean isBinarySearchTree(Node root);

    /**
     * This method displays Zig-Zag traversal logic.
     * @return ArrayList containing Zig-Zar traversed items.
     */
    ArrayList<Integer> zigZagTraversal(Node root);
}
