package com.denchsolutions.binarytree.service;

import com.denchsolutions.binarytree.model.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class BinaryTreeServiceImpl implements BinaryTreeService{

    @Override
    public boolean isBinarySearchTree(Node root) {
        if (root == null) {
            return true;
        }
        return checkAllNodes(root, null, null);
    }

    private boolean checkAllNodes(Node parent, Node left, Node right) {
        if (parent == null) {
            return true;
        }
        if (left != null && parent.getData() <= left.getData()) {
            return false;
        }
        if (right != null && parent.getData() >= right.getData()) {
            return false;
        }

        return checkAllNodes(parent.getLeft(), left, parent) && checkAllNodes(parent.getRight(), parent, right);
    }

    @Override
    public ArrayList<Integer> zigZagTraversal(Node root) {
        ArrayList<Integer> ans = new ArrayList<>();
        if (root == null) {
            return ans;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        boolean leftToRight = true;

        while (!queue.isEmpty()) {
            int size = queue.size();
            ArrayList<Integer> temp = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Node node = queue.poll();
                assert node != null;
                if (node.getLeft() != null) {
                    queue.add(node.getLeft());
                }
                if (node.getRight() != null) {
                    queue.add(node.getRight());
                }
                temp.add(node.getData());
            }
            if (!leftToRight) {
                Collections.reverse(temp);
            }
            ans.addAll(temp);
            leftToRight = !(leftToRight);
        }

        return ans;
    }
}
