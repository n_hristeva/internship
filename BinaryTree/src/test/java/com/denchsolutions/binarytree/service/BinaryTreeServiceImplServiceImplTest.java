package com.denchsolutions.binarytree.service;

import com.denchsolutions.binarytree.model.Node;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BinaryTreeServiceImplServiceImplTest {
    @Test
    public void isBinarySearchTree_expectTrue() {
        BinaryTreeService binaryTreeService = new BinaryTreeServiceImpl();

        Node isBSTRoot = new Node(4);
        isBSTRoot.setLeft(new Node(2));
        isBSTRoot.setRight(new Node(5));
        isBSTRoot.getLeft()
            .setLeft(new Node(1));
        isBSTRoot.getLeft()
            .setRight(new Node(3));

        assertTrue(binaryTreeService.isBinarySearchTree(isBSTRoot));
    }

    @Test
    public void isBinarySearchTree_expectFalse() {
        BinaryTreeService binaryTreeService = new BinaryTreeServiceImpl();

        Node isNotBSTRoot = new Node(3);
        isNotBSTRoot.setLeft(new Node(2));
        isNotBSTRoot.setRight(new Node(5));
        isNotBSTRoot.getLeft()
            .setLeft(new Node(1));
        isNotBSTRoot.getLeft()
            .setRight(new Node(4));

        assertFalse(binaryTreeService.isBinarySearchTree(isNotBSTRoot));
    }
}