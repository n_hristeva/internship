package com.denchsolutions.genericexecution;

import com.denchsolutions.genericexecution.model.ExecutionResult;
import com.denchsolutions.genericexecution.model.MultiplyTaskExecutor;
import com.denchsolutions.genericexecution.model.StringAppendExecutor;

public class Application {
    private static final int STRING_MAX_LENGTH = 128;

    public static void main(String[] args) {

        runMultiplyTaskExecutor();
        runStringAppendExecutor();
    }

    private static void runMultiplyTaskExecutor() {
        Long lMax = Long.MAX_VALUE;
        Long l = 2L;

        ExecutionResult<Long> er = MultiplyTaskExecutor.multiply(l, lMax);
        System.out.println(er);
    }

    private static void runStringAppendExecutor() {
        String s1 = " ".repeat(STRING_MAX_LENGTH);
        String s2 = " ";

        ExecutionResult<String> er = StringAppendExecutor.append(s1, s2);
        System.out.println(er);
    }
}


