package com.denchsolutions.genericexecution.model;

public class StringAppendExecutor extends ExecutionResult<String> {
    private static final int STRING_MAX_LENGTH = 128;
    private static final String LENGTH_GREATER_THAN_ERR_MSG = String.format("Exceeded allowed String length. Max %d.", STRING_MAX_LENGTH);

    private StringAppendExecutor(boolean success, String errorMessage, String result) {
        super(success, errorMessage, result);
    }

    public static ExecutionResult<String> append(String string1, String string2) {
        boolean success = true;
        String errorMessage = "";
        String result = string1 + string2;

        if (result.length() > STRING_MAX_LENGTH) {
            errorMessage = LENGTH_GREATER_THAN_ERR_MSG;
            success = false;
            result = null;
        }

        return new StringAppendExecutor(success, errorMessage, result);
    }
}

