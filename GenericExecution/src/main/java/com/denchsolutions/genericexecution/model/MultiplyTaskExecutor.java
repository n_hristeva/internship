package com.denchsolutions.genericexecution.model;

public class MultiplyTaskExecutor extends ExecutionResult<Long> {

    private MultiplyTaskExecutor(boolean success, String errorMessage, Long result) {
        super(success, errorMessage, result);
    }

    public static ExecutionResult<Long> multiply(Long number1, Long number2) {
        boolean success = true;
        String errorMessage = "";
        Long result = null;

        try {
            result = Math.multiplyExact(number1, number2);
        } catch (ArithmeticException ae) {
            success = false;
            errorMessage = ae.getMessage();
        }

        return new MultiplyTaskExecutor(success, errorMessage, result);
    }
}
