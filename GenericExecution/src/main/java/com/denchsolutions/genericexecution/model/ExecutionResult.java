package com.denchsolutions.genericexecution.model;

public abstract class ExecutionResult<T> {
    private static final String OUTPUT_MESSAGE = "Message: %s \nSuccess: %s \nResult:  %s\n";

    private final boolean success;
    private final String errorMessage;
    private final T result;

    protected ExecutionResult(boolean success, String errorMessage, T result) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean getSuccess() {
        return success;
    }

    public T getResult() {
        return result;
    }

    @Override
    public String toString() {
        return String.format(OUTPUT_MESSAGE, errorMessage, success, result);
    }
}

