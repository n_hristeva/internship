package com.denchsolutions.toiletsimulator.model;

import com.denchsolutions.toiletsimulator.enums.GenderType;

import java.sql.Timestamp;
import java.util.concurrent.locks.ReentrantLock;

public class Toilet {
    ReentrantLock lock = new ReentrantLock();
    private final GenderType genderType;

    public Toilet(GenderType genderType) {
        this.genderType = genderType;
    }

    public GenderType getGenderType() {
        return genderType;
    }

    public synchronized void use(int getTimeOutInSeconds) throws InterruptedException {
        Thread thread = Thread.currentThread();
        try {
            lock.lock();
            System.out.printf("%s is locked and sleeps in Toilet %s at time %s%n", thread.getName(), getGenderType(),
                              new Timestamp(System.currentTimeMillis()));
            Thread.sleep(getTimeOutInSeconds);
        } finally {
            System.out.printf("%s is unlocked Toilet is free %s at time %s %n%n", thread.getName(), getGenderType(),
                              new Timestamp(System.currentTimeMillis()));
            lock.unlock();
        }
    }
}
