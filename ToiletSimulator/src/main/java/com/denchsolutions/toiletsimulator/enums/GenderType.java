package com.denchsolutions.toiletsimulator.enums;

public enum GenderType {

    F("Female"), M("Male");

    private final String gender;

    public String getGender() {
        return gender;
    }

    GenderType(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return this.getGender();
    }
}
