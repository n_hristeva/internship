package com.denchsolutions.toiletsimulator;

import com.denchsolutions.toiletsimulator.enums.GenderType;
import com.denchsolutions.toiletsimulator.model.Toilet;
import com.denchsolutions.toiletsimulator.thread.CustomerThread;

public class Application {
    public static void main(String[] args) {
        Toilet toiletForWomen = new Toilet(GenderType.F);
        Toilet toiletForMan = new Toilet(GenderType.M);

        CustomerThread customerThreadMan1 = new CustomerThread(GenderType.M, toiletForMan);
        CustomerThread customerThreadMan2 = new CustomerThread(GenderType.M, toiletForMan);
        CustomerThread customerThreadMan3 = new CustomerThread(GenderType.M, toiletForMan);

        CustomerThread customerThreadWomen1 = new CustomerThread(GenderType.F, toiletForWomen);
        CustomerThread customerThreadWomen2 = new CustomerThread(GenderType.F, toiletForWomen);
        CustomerThread customerThreadWomen3 = new CustomerThread(GenderType.F, toiletForWomen);

        customerThreadMan1.start();
        customerThreadMan2.start();
        customerThreadMan3.start();

        customerThreadWomen1.start();
        customerThreadWomen2.start();
        customerThreadWomen3.start();
    }
}
