package com.denchsolutions.toiletsimulator.thread;

import com.denchsolutions.toiletsimulator.enums.GenderType;
import com.denchsolutions.toiletsimulator.model.Toilet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class CustomerThread extends Thread {
    private static final Logger log = LoggerFactory.getLogger(CustomerThread.class);
    private final GenderType genderType;
    private final Toilet toilet;
    private int timeOutInSeconds;

    public GenderType getGenderType() {
        return genderType;
    }

    public Toilet getToilet() {
        return toilet;
    }

    public CustomerThread(GenderType genderType, Toilet toilet) {
        this.genderType = genderType;
        this.toilet = toilet;
        this.setTimeOutInSeconds(genderType);
    }

    private void setTimeOutInSeconds(GenderType genderType) {
        if (genderType == GenderType.M) {
            this.timeOutInSeconds = 1000;
        } else if (genderType == GenderType.F) {
            this.timeOutInSeconds = 2000;
        }
    }

    public int getTimeOutInSeconds() {
        return timeOutInSeconds;
    }

    @Override
    public void run() {
        useToilet();
        super.run();
    }

    private synchronized void useToilet() {
        System.out.println(
            String.format("%s with gender %s will be locked to sleep in Toilet %s %n", getName(), getGenderType(), getToilet().getGenderType()));
        try {
            toilet.use(getTimeOutInSeconds());
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            interrupt();
        }
    }
}