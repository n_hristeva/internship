package com.denchsolutions.genericarrayblockingqueue.thread;

import com.denchsolutions.genericarrayblockingqueue.model.BlockingStructure;

public final class ConsumerThread<T> extends Thread {
    private final BlockingStructure<T> blockingStructure;

    public ConsumerThread(BlockingStructure<T> blockingStructure) {
        this.blockingStructure = blockingStructure;
    }

    @Override
    public void run() {
        while (blockingStructure.size() < 1) {
            synchronized (blockingStructure) {
                this.blockingStructure.take();
            }
        }
    }
}