package com.denchsolutions.genericarrayblockingqueue.thread;

import com.denchsolutions.genericarrayblockingqueue.model.BlockingStructure;
import com.denchsolutions.genericarrayblockingqueue.model.ConsoleReader;

public final class ProducerThread<T> extends Thread {
    private final BlockingStructure<T> blockingStructure;
    private final ConsoleReader<T> consoleReader;

    public ProducerThread(BlockingStructure<T> blockingStructure, ConsoleReader<T> consoleReader) {
        this.blockingStructure = blockingStructure;
        this.consoleReader = consoleReader;
    }

    @Override
    public void run() {
        for (int i = 1; i <= blockingStructure.getLimit(); i++) {
            final T t = consoleReader.read();
            blockingStructure.put(t);
        }
    }
}
