package com.denchsolutions.genericarrayblockingqueue.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static java.lang.System.out;

public class MapConsoleReader implements ConsoleReader<Pair<Integer, Integer>> {
    private static final Logger log = LoggerFactory.getLogger(MapConsoleReader.class);
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public Pair<Integer, Integer> read() {
        String[] getData = new String[1];
        try {
            getData = reader.readLine()
                .split("-");
            out.printf("reads %s : %s%n", getData[0], getData[1]);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        return new Pair<>(Integer.parseInt(getData[0]), Integer.parseInt(getData[1]));
    }
}
