package com.denchsolutions.genericarrayblockingqueue.model;

public interface ConsoleReader<T> {

    /**
     * This generic method reads from the console.
     *
     * @return Generic type <T>
     */
    T read();
}
