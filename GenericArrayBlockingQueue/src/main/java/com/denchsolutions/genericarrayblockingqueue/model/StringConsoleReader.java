package com.denchsolutions.genericarrayblockingqueue.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static java.lang.System.out;

public class StringConsoleReader implements ConsoleReader<String> {
    private static final Logger log = LoggerFactory.getLogger(StringConsoleReader.class);
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String read() {
        String getData = "";
        try {
            getData = reader.readLine();
            out.println("reads : " + getData);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        return getData;
    }
}
