package com.denchsolutions.genericarrayblockingqueue.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

import static java.lang.System.out;

public class ArrayBlockingQueue<T> implements BlockingStructure<T> {
    private static final Logger log = LoggerFactory.getLogger(ArrayBlockingQueue.class);

    private final LinkedList<T> queue = new LinkedList<>();
    private final int limit;

    public ArrayBlockingQueue(Integer limit) {
        this.limit = limit;
    }

    @Override
    public int getLimit() {
        return limit;
    }

    @Override
    public synchronized int size() {
        return this.queue.size();
    }

    @Override
    public synchronized void put(T t) {
        Thread thread = Thread.currentThread();
        while (this.queue.size() == this.limit) {
            try {
                wait();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
                Thread.currentThread()
                    .interrupt();
            }
        }
        String s = String.format("'Put' element '%s' from queue by thread Id/Name %d/%s", t, thread.getId(), thread.getName());
        out.println(s);
        this.queue.add(t);
        if (this.queue.size() == 1) {
            notifyAll();
        }
    }

    @Override
    public synchronized void take() {
        Thread thread = Thread.currentThread();
        while (this.queue.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
                thread.interrupt();
            }
        }
        if (this.queue.size() == this.limit) {
            notifyAll();
        }
        String s = String.format("'Take' from Thread Id/Name %d/%s and <data> is %s", thread.getId(), thread.getName(), this.queue.get(0));
        out.println(s);

        this.queue.remove(0);
    }
}
