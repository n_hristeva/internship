package com.denchsolutions.genericarrayblockingqueue.model;

public interface BlockingStructure<T> {

    /**
     * Puts an element in end of the Tail of a Queue.
     *
     * @param t is the element that will be added.
     */
    void put(T t);

    /**
     * Getter of a private field limit.
     * Member limit passes thought to the constructors.
     * Stands for the possible max size of the structure
     *
     * @return an integer representing the limit.
     */
    int getLimit();

    /**
     * Gets current size of the structure.
     *
     * @return an integer representing the current size of the structure.
     */
    int size();

    /**
     * Example:
     * If the Blocking Structure is type Map 'E e' can be referred as first key in Map.
     * If the Blocking Structure is type List 'E e' can refer to an index. In this case should be always first - head of List/Queue.
     */
    void take();
}
