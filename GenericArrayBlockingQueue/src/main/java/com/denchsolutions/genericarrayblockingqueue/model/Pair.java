package com.denchsolutions.genericarrayblockingqueue.model;

public record Pair<K, V>(K k, V v) {

    public K getKey() {
        return k;
    }

    public V getValue() {
        return v;
    }

}
