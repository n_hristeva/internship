package com.denchsolutions.genericarrayblockingqueue.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;

import static java.lang.System.out;

public class BlockingMap implements BlockingStructure<Pair<Integer, Integer>> {
    private static final Logger log = LoggerFactory.getLogger(BlockingMap.class);

    private final Map<Integer, Integer> map = new LinkedHashMap<>();
    private final int limit;

    public BlockingMap(int limit) {
        this.limit = limit;
    }

    @Override
    public int getLimit() {
        return limit;
    }

    @Override
    public synchronized int size() {
        return this.map.size();
    }

    @Override
    public synchronized void put(Pair<Integer, Integer> element) {
        Thread thread = Thread.currentThread();
        Integer key = element.getKey();
        Integer value = element.getValue();
        while (this.map.size() == this.limit) {
            try {
                wait();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
                Thread.currentThread()
                    .interrupt();
            }
        }
        String s =
            String.format("'Put' element Key '%s' with value '%s' from queue by thread Id/Name %d/%s", key, value, thread.getId(), thread.getName());
        out.println(s);

        Integer newValue = value + (this.map.getOrDefault(key, 0));
        this.map.put(key, newValue);

        if (this.map.size() == 1) {
            notifyAll();
        }
    }

    @Override
    public synchronized void take() {
        Thread thread = Thread.currentThread();
        while (this.map.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
                thread.interrupt();
            }
        }
        if (this.map.size() == this.limit) {
            notifyAll();
        }
        Integer firstKey = (Integer) this.map.keySet()
            .toArray()[0];
        Integer firstKeyValue = this.map.get(firstKey);
        String s = String.format("'Take' from Thread Id/Name %d/%s and <data> is %s : %s", thread.getId(), thread.getName(), firstKey, firstKeyValue);
        out.println(s);

        this.map.remove(firstKey);
    }
}
