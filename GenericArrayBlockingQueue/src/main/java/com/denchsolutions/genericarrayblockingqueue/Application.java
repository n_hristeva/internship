package com.denchsolutions.genericarrayblockingqueue;

import com.denchsolutions.genericarrayblockingqueue.model.ArrayBlockingQueue;
import com.denchsolutions.genericarrayblockingqueue.model.BlockingMap;
import com.denchsolutions.genericarrayblockingqueue.model.BlockingStructure;
import com.denchsolutions.genericarrayblockingqueue.model.ConsoleReader;
import com.denchsolutions.genericarrayblockingqueue.model.MapConsoleReader;
import com.denchsolutions.genericarrayblockingqueue.model.Pair;
import com.denchsolutions.genericarrayblockingqueue.model.StringConsoleReader;
import com.denchsolutions.genericarrayblockingqueue.thread.ConsumerThread;
import com.denchsolutions.genericarrayblockingqueue.thread.ProducerThread;

public class Application {
    public static void main(String[] args) {
        Integer numberOfConsumers = getNumberOfConsumers(args);

        BlockingStructure<String> arrayBlockingQueue = new ArrayBlockingQueue<>(numberOfConsumers);
        ConsoleReader<String> stringConsoleReader = new StringConsoleReader();

        (new ProducerThread<>(arrayBlockingQueue, stringConsoleReader)).start();
        (new ProducerThread<>(arrayBlockingQueue, stringConsoleReader)).start();
        (new ProducerThread<>(arrayBlockingQueue, stringConsoleReader)).start();

        (new ConsumerThread<>(arrayBlockingQueue)).start();
        (new ConsumerThread<>(arrayBlockingQueue)).start();

        BlockingStructure<Pair<Integer, Integer>> blockingMap = new BlockingMap(numberOfConsumers);
        ConsoleReader<Pair<Integer, Integer>> mapConsoleReader = new MapConsoleReader();

        (new ProducerThread<>(blockingMap, mapConsoleReader)).start();
        (new ProducerThread<>(blockingMap, mapConsoleReader)).start();
        (new ProducerThread<>(blockingMap, mapConsoleReader)).start();
    }

    private static Integer getNumberOfConsumers(String[] args) {
        int result = 0;
        if (args.length == 1) {
            result = Integer.parseInt(args[0]);
        }

        return result;
    }
}
