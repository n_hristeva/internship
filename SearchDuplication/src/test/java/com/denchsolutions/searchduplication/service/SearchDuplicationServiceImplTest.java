package com.denchsolutions.searchduplication.service;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class SearchDuplicationServiceImplTest {

    private static final ArrayList<String> SIMULATE_USER_INPUT = new ArrayList<>(Arrays.asList("diff", "1", "5", "1.0", "1", "55", "diff"));
    private static final Set<String> EXPECTED_RESULT = new HashSet<>(Arrays.asList("diff", "1"));

    @Test
    public void findDuplicatedElements_correctInput_correctOutput() {

        final SearchDuplicationService searchDuplicationService = new SearchDuplicationServiceImpl();
        final Set<String> findDuplicatedElements = searchDuplicationService.findDuplicatedElements(SIMULATE_USER_INPUT);
        assertEquals(EXPECTED_RESULT, findDuplicatedElements);
    }
}