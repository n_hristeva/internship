package com.denchsolutions.searchduplication;

import com.denchsolutions.searchduplication.service.SearchDuplicationService;
import com.denchsolutions.searchduplication.service.SearchDuplicationServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class Application {
    private static final String MORE_PARAMETERS_REQUIRED_ERR = "Entered values must be at least two.";
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        try {
            final ArrayList<String> listToSearch = readInput(args);
            final SearchDuplicationService searchDuplicationService = new SearchDuplicationServiceImpl();
            final Set<String> duplicatedElements = searchDuplicationService.findDuplicatedElements(listToSearch);

            printResult(duplicatedElements);
        } catch (IllegalArgumentException iae) {
            log.error(iae.getMessage());
        }
    }

    private static ArrayList<String> readInput(final String[] args) throws IllegalArgumentException {
        if (args.length <= 1) {
            throw new IllegalArgumentException(MORE_PARAMETERS_REQUIRED_ERR);
        }

        return new ArrayList<>(Arrays.asList(args));
    }

    private static void printResult(final Set<String> duplicatedElements) {
        for (String e : duplicatedElements) {
            System.out.println(e);
        }
    }
}


