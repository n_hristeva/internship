package com.denchsolutions.searchduplication.service;

import java.util.List;
import java.util.Set;

public interface SearchDuplicationService {
    /**
     * Searches for duplicated elements in the provided ArrayList.
     *
     * @param listToSearch contains 'x' amount Strings.
     * @return a Set type of Collection containing all duplicated elements
     */
    Set<String> findDuplicatedElements(List<String> listToSearch);
}
