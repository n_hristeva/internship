package com.denchsolutions.searchduplication.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class SearchDuplicationServiceImpl implements SearchDuplicationService {

    @Override
    public Set<String> findDuplicatedElements(final List<String> listToSearch) {
        Set<String> setWithDuplicates = new HashSet<>();
        Set<String> setWithUniques = new HashSet<>();
        for (String element : listToSearch) {
            if (!setWithUniques.add(element)) {
                setWithDuplicates.add(element);
            }
        }

        return setWithDuplicates;
    }
}
