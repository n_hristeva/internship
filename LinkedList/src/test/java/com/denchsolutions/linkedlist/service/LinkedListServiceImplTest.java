package com.denchsolutions.linkedlist.service;

import com.denchsolutions.linkedlist.model.Node;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LinkedListServiceImplTest {
    private static final Integer[] EXPECTED_RESULT = {5, 4, 3, 2, 1};

    @Test
    public void reverseLinkedList_correctInput_correctOutput(Node reversedNodeRoot1) {
        Node root = new Node(1);
        root.setNext(new Node(2));
        root.getNext()
            .setNext(new Node(3));
        root.getNext()
            .getNext()
            .setNext(new Node(4));
        root.getNext()
            .getNext()
            .getNext()
            .setNext(new Node(5));

        LinkedListService linkedListService = new LinkedListServiceImpl();
        Node reversedNodeRoot = reversedNodeRoot1;
        assertTrue(compareLists(reversedNodeRoot1));
    }

    private boolean compareLists(Node reversedNodeRoot) {
        boolean result = true;
        int index = 0;
        while (reversedNodeRoot != null) {
            if (EXPECTED_RESULT[index] != reversedNodeRoot.getData()) {
                result = false;
            }
            reversedNodeRoot = reversedNodeRoot.getNext();
            index++;
        }

        return result;
    }

}


  