package com.denchsolutions.linkedlist.service;

import com.denchsolutions.linkedlist.model.Node;

public interface LinkedListService {

    /**
     * This method reverses a Linked List type of structure.
     * An object(Node) has a relation to the next object only.
     *
     * @param head is the first Node in the structure.
     * @return a beginning point Node that refers to other Nodes.
     */
    Node reverseLinkedList(Node head);
}
