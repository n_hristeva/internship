package com.denchsolutions.linkedlist.service;

import com.denchsolutions.linkedlist.model.Node;

public class LinkedListServiceImpl implements LinkedListService {

    @Override
    public Node reverseLinkedList(Node node) {

        return reverse(node, node);
    }

    private Node reverse(Node head, Node headReference) {
        Node first, tail;
        if (head == null) {
            return headReference;
        }
        first = head;
        tail = first.getNext();

        if (tail == null) {
            headReference = first;
            return headReference;
        }

        headReference = reverse(tail, headReference);
        tail.setNext(first);
        first.setNext(null);

        return headReference;
    }
}
