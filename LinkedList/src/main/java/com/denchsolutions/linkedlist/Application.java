package com.denchsolutions.linkedlist;

import com.denchsolutions.linkedlist.model.Node;
import com.denchsolutions.linkedlist.service.LinkedListService;
import com.denchsolutions.linkedlist.service.LinkedListServiceImpl;

public class Application {
    public static void main(String[] args) {

        Node root = new Node(1);
        root.setNext(new Node(2));
        root.getNext()
            .setNext(new Node(3));
        root.getNext()
            .getNext()
            .setNext(new Node(4));
        root.getNext()
            .getNext()
            .getNext()
            .setNext(new Node(5));


        LinkedListService linkedListService = new LinkedListServiceImpl();
        printBinaryTree(linkedListService.reverseLinkedList(root));
    }

    private static void printBinaryTree(Node node) {
        while (node != null) {
            System.out.print(node.getData() + " ");
            node = node.getNext();
        }
    }
}
