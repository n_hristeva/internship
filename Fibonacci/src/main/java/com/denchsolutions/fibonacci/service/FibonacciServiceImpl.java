package com.denchsolutions.fibonacci.service;

import java.math.BigInteger;

public final class FibonacciServiceImpl implements FibonacciService {

    @Override
    public BigInteger[] fibonacciWithFor(int number) {
        BigInteger[] result = new BigInteger[number];
        BigInteger firstTerm = new BigInteger(String.valueOf(0));
        BigInteger secondTerm = new BigInteger(String.valueOf(1));

        for (int i = 0; i < number; ++i) {
            result[i] = firstTerm;
            BigInteger nextTerm = new BigInteger(String.valueOf(firstTerm.add(secondTerm)));
            firstTerm = secondTerm;
            secondTerm = nextTerm;
        }

        return result;
    }

    @Override
    public BigInteger[] fibonacciWithRecursion(int number) {
        BigInteger[] result = new BigInteger[number];
        fibonacciRecursionMemorySolution(number - 1, result);

        return result;
    }

    private static BigInteger fibonacciRecursionMemorySolution(int num, BigInteger[] result) {
        if (num <= 1) {
            result[num] = BigInteger.valueOf(num);
            return BigInteger.valueOf(num);
        }
        if (result[num] == null) {
            result[num] = fibonacciRecursionMemorySolution(num - 1, result).add(fibonacciRecursionMemorySolution(num - 2, result));
        }

        return result[num];
    }
}
