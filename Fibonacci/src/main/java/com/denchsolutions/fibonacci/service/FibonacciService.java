package com.denchsolutions.fibonacci.service;

import java.math.BigInteger;

public interface FibonacciService {

    /**
     * **Calculates Fibonacci using for-loop.
     *
     * @param number is the count of the required Fibonacci numbers.
     * @return an Array of length 'number', filled-up with Fibonacci numbers.
     */
    BigInteger[] fibonacciWithFor(int number);

    /**
     * *Calculates Fibonacci using recursion.
     *
     * @param number is the count of the required Fibonacci numbers.
     * @return an Array of length 'number', filled-up with Fibonacci numbers.
     */
    BigInteger[] fibonacciWithRecursion(int number);
}
