package com.denchsolutions.fibonacci;

import com.denchsolutions.fibonacci.service.FibonacciService;
import com.denchsolutions.fibonacci.service.FibonacciServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.Arrays;

public class Application {
    private static final String SINGLE_PARAMETER_REQUIRED_ERR = "Enter only one value.";
    private static final String PARSE_TO_INT_ERR = "Argument '{}' can not be parsed to integer.";
    private static final String UNDER_AND_ZERO_ERR = "Entered value can not be under zero.";

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        try {
            final int number = readInput(args);
            final FibonacciService fibonacciService = new FibonacciServiceImpl();
            final BigInteger[] fibonacciNumbers = fibonacciService.fibonacciWithRecursion(number);

            System.out.println(Arrays.toString(fibonacciNumbers));
        } catch (NumberFormatException nfe) {
            log.error(PARSE_TO_INT_ERR, args[0]);
        } catch (IllegalArgumentException iae) {
            log.error(iae.getMessage());
        }
    }

    private static int readInput(final String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            throw new IllegalArgumentException(SINGLE_PARAMETER_REQUIRED_ERR);
        }

        final int number = Integer.parseInt(args[0]);

        if (number <= 0) {
            throw new IllegalArgumentException(UNDER_AND_ZERO_ERR);
        }

        return number;
    }
}
