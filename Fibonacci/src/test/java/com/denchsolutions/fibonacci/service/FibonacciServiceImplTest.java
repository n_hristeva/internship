package com.denchsolutions.fibonacci.service;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertArrayEquals;

public class FibonacciServiceImplTest {

    private static final int REQUESTED_FIBONACCI_NUMBERS = 5;
    private static final BigInteger[] EXPECTED_RESULT =
        {BigInteger.valueOf(0), BigInteger.valueOf(1), BigInteger.valueOf(1), BigInteger.valueOf(2), BigInteger.valueOf(3)};

    final FibonacciService fibonacciService = new FibonacciServiceImpl();

    @Test
    public void fibonacciWithFor_correctInput_correctOutput() {
        final BigInteger[] fibonacciNumbers = fibonacciService.fibonacciWithFor(REQUESTED_FIBONACCI_NUMBERS);
        assertArrayEquals(EXPECTED_RESULT, fibonacciNumbers);
    }

    @Test
    public void fibonacciWithRecursion_correctInput_correctOutput() {
        final BigInteger[] fibonacciNumbers = fibonacciService.fibonacciWithRecursion(REQUESTED_FIBONACCI_NUMBERS);
        assertArrayEquals(EXPECTED_RESULT, fibonacciNumbers);
    }
}
