package com.denchsolutions.employees.service;

import com.denchsolutions.employees.model.Employee;
import com.denchsolutions.employees.model.QEmployee;
import com.denchsolutions.employees.repository.EmployeeRepository;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @PersistenceContext
    EntityManager em;
    private static final QEmployee qEmployee = QEmployee.employee;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> listEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Optional<Employee> searchEmployeesById(Integer id) {
        return employeeRepository.findById(id);
    }

    @Override
    @Query
    public List<Employee> searchEmployeesByName(String name) {
        JPAQuery<Employee> query = new JPAQuery<>(em);
        List<Employee> employees = query.select(qEmployee)
            .from(qEmployee)
            .where(qEmployee.name.eq(name))
            .fetch();
        if (employees.isEmpty()) {
            return new ArrayList<>();
        } else {
            return employees;
        }
    }

    @Override
    @Query
    public List<Employee> searchEmployeesBySalary(Double salary) {
        JPAQuery<Employee> query = new JPAQuery<>(em);
        List<Employee> employees = query.select(qEmployee)
            .from(qEmployee)
            .where(qEmployee.salary.eq(salary))
            .fetch();
        if (employees.isEmpty()) {
            return new ArrayList<>();
        } else {
            return employees;
        }
    }


}
