package com.denchsolutions.employees.service;

import com.denchsolutions.employees.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    /**
     * @return all Employees from the base.
     */
    List<Employee> listEmployees();

    /**
     * @param id search for Employee by id.
     * @return One Employee with specific id.
     */
    Optional<Employee> searchEmployeesById(Integer id);

    /**
     * @param name search for Employees by Name.
     * @return List of Employee/s with entered Name value.
     */
    List<Employee> searchEmployeesByName(String name);

    /**
     * @param salary search for Employees by Salary.
     * @return List of Employee/s with entered salary amount.
     */
    List<Employee> searchEmployeesBySalary(Double salary);
}
