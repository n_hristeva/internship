package com.denchsolutions.employees.controller;

import com.denchsolutions.employees.model.Employee;
import com.denchsolutions.employees.service.EmployeeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RestController
public class MainRestController {
    @Resource()
    private final EmployeeService employeeService;

    MainRestController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/hello-world")
    public String helloWorld() {
        return "Hello!";
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeService.listEmployees();
    }

    @GetMapping("/employee/{id}")
    public Optional<Employee> getEmployeeById(@PathVariable Integer id) {
        return employeeService.searchEmployeesById(id);
    }

    @GetMapping("/employee/name")
    public List<Employee> getAllEmployeesByName(@RequestParam String name) {
        return employeeService.searchEmployeesByName(name);
    }

    @GetMapping("/employee/salary")
    public List<Employee> getAllEmployeesBySalary(@RequestParam Double salary) {
        return employeeService.searchEmployeesBySalary(salary);
    }
}
