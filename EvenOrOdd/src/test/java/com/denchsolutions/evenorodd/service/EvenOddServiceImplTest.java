package com.denchsolutions.evenorodd.service;

import com.denchsolutions.evenorodd.enums.NumberType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EvenOddServiceImplTest {
    private final static double EVEN_NUMBER = 2.0;
    private final static double ODD_NUMBER = 3.0;
    
    private final EvenOddService evenOddService = EvenOddServiceImpl.getInstance();

    @Test
    public void getInstance_checkIfReturnsNull() {
        assertNotNull(evenOddService);
    }

    @Test
    public void evenOrOdd_checkEvenNumberInput() {
        final NumberType mustBeEven = evenOddService.evenOrOdd(EVEN_NUMBER);
        assertEquals(NumberType.EVEN, mustBeEven);
    }

    @Test
    public void evenOrOdd_checkOddNumberInput() {
        final NumberType mustBeOdd = evenOddService.evenOrOdd(ODD_NUMBER);
        assertEquals(NumberType.ODD, mustBeOdd);
    }
}