package com.denchsolutions.evenorodd;

import com.denchsolutions.evenorodd.service.EvenOddService;
import com.denchsolutions.evenorodd.service.EvenOddServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Application {

    private static final String SINGLE_PARAMETER_REQUIRED_ERR = "Enter only one value.";
    private static final String PARSE_TO_DOUBLE_ERR = "Argument '{}' can not be parsed to double.";
    private static final String FINAL_MESSAGE = "The result is '%s' from provided number.";

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            final double number = readInput(args); // in the explanation we do not know is we need to check int or double
            final EvenOddService evenOddService = EvenOddServiceImpl.getInstance();

            System.out.printf(FINAL_MESSAGE, evenOddService.evenOrOdd(number)
                .toString());
        } catch (NumberFormatException nfe) {
            log.error(PARSE_TO_DOUBLE_ERR, args[0]);
        } catch (IllegalArgumentException iae) {
            log.error(iae.getMessage());
        }
    }

    private static double readInput(final String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            throw new IllegalArgumentException(SINGLE_PARAMETER_REQUIRED_ERR);
        }

        return Double.parseDouble(args[0]);
    }
}
