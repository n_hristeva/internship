package com.denchsolutions.evenorodd.service;

import com.denchsolutions.evenorodd.enums.NumberType;

public interface EvenOddService {

    /**
     * Finds whether a number is even or odd
     *
     * @param number is an input value, this value is checked if it is 'even' or 'odd'
     * @return enumeration - NumberType
     */
    NumberType evenOrOdd(double number);
}
