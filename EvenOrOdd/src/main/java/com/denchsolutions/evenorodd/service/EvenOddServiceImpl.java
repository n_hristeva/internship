package com.denchsolutions.evenorodd.service;

import com.denchsolutions.evenorodd.enums.NumberType;

public final class EvenOddServiceImpl implements EvenOddService {
    private static EvenOddServiceImpl instance = null;

    private EvenOddServiceImpl() {
    }

    public static EvenOddServiceImpl getInstance() {
        if (instance == null) {
            instance = new EvenOddServiceImpl();
        }

        return instance;
    }

    @Override
    public NumberType evenOrOdd(final double number) {
        NumberType result;

        if (number % 2 == 0) {
            result = NumberType.EVEN;
        } else {
            result = NumberType.ODD;
        }

        return result;
    }
}

