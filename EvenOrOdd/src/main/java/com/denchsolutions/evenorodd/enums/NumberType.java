package com.denchsolutions.evenorodd.enums;


public enum NumberType {
    EVEN("even"), ODD("odd");

    private final String value;

    NumberType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
